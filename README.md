## Example libpfprint driver using libfprint-TOD (libfprint for Touch OEM Drivers)

This example driver is supposed to be compiled using [libfprint-tod](https://gitlab.freedesktop.org/3v1n0/libfprint/tree/tod).

### Compile

Once that libfprint light-fork is installed in your prefix, just use

    meson _build
    ninja -C _build
    # Optional, will install this in the default libfprint-tod drivers path
    ninja -C _build install


### Use and Develop

The easiest way to test the driver is to use the libfprint example tools, so from the libfprint `$BUILD/examples` directory just run

    env FP_TOD_DRIVER_EXAMPLE=1 ./enroll

If you don't want to install this driver, you can use the `FP_TOD_DRIVERS_DIR` env variable to define the path of the directory form where the `libdevice-fake-tod-driver.so` that will be loaded (so can be just pointed to the build directory of this repository).
